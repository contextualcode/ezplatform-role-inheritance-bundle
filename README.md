# eZ Platform Role Inheritance Bundle
This is an eZ Platform bundle which provides user role inheritance.

This is different from the built-in User Group inheritance because
it allows for fine-grained, easy management of user roles that doesn't depend on groups.

(This is an eZ Platform port of [permissions-inheritance-bundle](https://gitlab.com/contextualcode/permissions-inheritance-bundle).)

## Installation
- Run `composer require`:

```bash
$ composer require contextualcode/ezplatform-role-inheritance-bundle
```
- Enable this bundle in `app/AppKernel.php` file by adding next line in `registerBundles` method:

```php
    public function registerBundles()
    {
        $bundles = array(
            ...
            new ContextualCode\EzPlatformRoleInheritanceBundle\ContextualCodeEzPlatformRoleInheritanceBundle(),
        );
```
- Add the routes to your `app/config/routing.yml`:
```yml
contextual_code_ez_platform_role_inheritance:
    resource: "@ContextualCodeEzPlatformRoleInheritanceBundle/Resources/config/routing.yml"
    prefix:   /
```
- Create new custom database tables:

```
$ cat vendor/contextualcode/ezplatform-role-inheritance-bundle/src/ContextualCode/EzPlatformRoleInheritanceBundle/SQL/MySQL/schema.sql | mysql -u <username -p<password> <database_name>
```

**Note**: If you are upgrading from an eZ 5.x install that was using [contextualcode/permissions-inheritance-bundle](https://packagist.org/packages/contextualcode/permissions-inheritance-bundle), 
please instead run the upgrade SQL to retain your old role inheritances:

```
$ cat vendor/contextualcode/ezplatform-role-inheritance-bundle/src/ContextualCode/EzPlatformRoleInheritanceBundle/SQL/MySQL/upgrade.sql | mysql -u <username -p<password> <database_name>
```

- Done.

## Usage
1. Just open the role view page in the admin UI. There will be new functionality to add parent/child roles, in the "Role Inheritances" tab.
2. Additional code needs to be executed to assign inherited roles to the user: you must call the function `handleUserChildRoles($userId)` of [RoleInheritanceService](https://gitlab.com/contextualcode/ezplatform-role-inheritance-bundle/blob/master/src/ContextualCode/RoleInheritanceBundle/Services/RoleInheritanceService.php).
You can use a custom login handler/event listener or any other way to call that code.
For example, you can create `AppBundle\Security\User\Provider.php` that extends `eZ\Publish\Core\MVC\Symfony\Security\User\Provider`.
Add the RoleInheritanceService to the constructor, and override the `refreshUser` function to add these lines to load the inherited roles:
```php
$userId = $user->getAPIUser()->contentInfo->id;
$this->roleInheritanceService->handleUserChildRoles($userId);
```
Then you must set `security.providers.ezpublish.id` to use the id of this new Provider. Now users will inherit roles set in the admin UI.
