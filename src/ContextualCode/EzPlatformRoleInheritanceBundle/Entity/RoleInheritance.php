<?php

namespace ContextualCode\EzPlatformRoleInheritanceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="cc_role_inheritance")
 */
class RoleInheritance
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", length=11, name="parent_role_id")
     */
    private $parentRoleId;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", length=11, name="child_role_id")
     */
    private $childRoleId;

    /**
     * @param int $parentRoleId
     */
    public function setParentRoleId(int $parentRoleId)
    {
        $this->parentRoleId = $parentRoleId;
    }

    /**
     * @return int
     */
    public function getParentRoleId() : int
    {
        return $this->parentRoleId;
    }

    /**
     * @param int $childRoleId
     */
    public function setChildRoleId(int $childRoleId)
    {
        $this->childRoleId = $childRoleId;
    }

    /**
     * @return int
     */
    public function getChildRoleId() : int
    {
        return $this->childRoleId;
    }

}