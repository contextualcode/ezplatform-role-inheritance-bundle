<?php

namespace ContextualCode\EzPlatformRoleInheritanceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="cc_user_role_inheritance")
 */
class UserRoleInheritance
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", length=11, name="user_id")
     */
    private $userId;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", length=11, name="child_role_id")
     */
    private $childRoleId;

    /**
     * @param int $userId
     */
    public function setUserId(int $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getUserId() : int
    {
        return $this->userId;
    }

    /**
     * @param int $childRoleId
     */
    public function setChildRoleId(int $childRoleId)
    {
        $this->childRoleId = $childRoleId;
    }

    /**
     * @return int
     */
    public function getChildRoleId() : int
    {
        return $this->childRoleId;
    }

}